/*
 * InputText Messages
 *
 * This contains all the text for the InputText component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.InputText';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the InputText component!',
  },
});
