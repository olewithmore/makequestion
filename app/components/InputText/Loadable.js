/**
 *
 * Asynchronously loads the component for InputText
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
