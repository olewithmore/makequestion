import React, { useState, useEffect, Fragment } from 'react';
import PropTypes from 'prop-types';

function someExpensiveComputation(initialCount) {
  console.log('someExpensiveComputation :');
  return initialCount * 2;
}

function Counter({ initialCount }) {
  const [count, setCount] = useState(() =>
    someExpensiveComputation(initialCount),
  );
  useEffect(() => {
    const subscription = setInterval(() => {
      console.log(new Date());
    }, 2000);
  });
  return (
    <Fragment>
      Count: {count}
      <button onClick={() => setCount(initialCount)}>Reset</button>
      <button onClick={() => setCount(prevCount => prevCount + 1)}>+</button>
      <button onClick={() => setCount(prevCount => prevCount)}>-</button>
    </Fragment>
  );
}

Counter.propTypes = {
  initialCount: PropTypes.number.isRequired,
};

export default Counter;
