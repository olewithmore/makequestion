class LinkListNode {
  constructor(data) {
    this.data = data;
    this.next = null;
  }
}

const head = Symbol('head');

class LinkList {
  constructor() {
    this[head] = null;
  }

  add(data) {
    const DATA = new LinkListNode(data);
    if (this[head] === null) {
      this[head] = DATA;
      return;
    }

    let current = this[head];
    while (current.next !== null) {
      current = current.next;
    }
    current.next = DATA;
  }

  get(index) {
    if (index < 0) {
      throw `Index ${index} is not exist`;
    }

    let i = 0;
    let current = this[head];
    while (i < index && current) {
      current = current.next;
      i += 1;
    }
    if (current) {
      return current.data;
    }
    return `Index ${index} is not exist`;
  }

  remove(index) {
    if (index < 0) {
      return `Index ${index} is not exist`;
    }

    if (index === 0 && this[head]) {
      const { data } = this[head];
      this[head] = this[head].next;
      return data;
    }

    let prev;
    let i = 0;
    let current = this[head];

    while (i < index && current) {
      prev = current;
      current = current.next;
      i++;
    }

    if (current) {
      const { data } = prev.next;
      prev.next = current.next;
      return `Remove ${index} of : ${data}`;
    }

    return false;
  }
}

const LIST = new LinkList();
LIST.add('cake');
LIST.add('ole');
LIST.add('JBL');
LIST.add('Sony');

const quickSort = arr => {
  if (!arr || arr.length === 0) {
    return [];
  }

  const midIdx = Math.floor(arr.length / 2);
  const left = [];
  const right = [];
  const pivot = arr[midIdx];

  arr.forEach((n, i) => {
    if (+midIdx === i) return;
    if (pivot > n) {
      left.push(n);
    } else {
      right.push(n);
    }
  });

  return [].concat(quickSort(left), [pivot], quickSort(right));
};

console.log('quick sort :', quickSort([7, 19, 2, 6, 4, 99, 31, 11, 25]));


const bubbleSort = () => {
  
}

